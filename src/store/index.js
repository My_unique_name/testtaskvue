import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VuexWebExtensions from 'vuex-webextensions'

Vue.use(Vuex)

export const store = new Vuex.Store({
  plugins: [VuexWebExtensions({
    persistentStates: ['stateone', 'statetwo'],
    loggerLevel: 'error'
  })],
  state: {
    sites: [],
    messageHandler: {},
    dataUrl: 'https://www.softomate.net/ext/employees/list.json'
  },
  mutations: {
    SET_SITES (state, sites) {
      Vue.set(state, 'sites', sites)
    },
    SET_MESSAGE_HANDLER (state, sites) {
      for (const site of sites) {
        if (!(site.domain in state.messageHandler)) {
          state.messageHandler[site.domain] = { count: 0, status: 'active' }
        }
      }
    },
    SET_BLOCK_MESSAGE_STATUS (state, site) {
      state.messageHandler[site.domain].status = 'blocked'
    },
    INCREASE_MESSAGE_COUNT (state, site) {
      state.messageHandler[site.domain].count += 1
    },
    CLEAR_MESSAGE_HANDLER (state) {
      state.sites.forEach(site => { state.messageHandler[site.domain] = { count: 0, status: 'active' } })
    }
  },
  actions: {
    async loadListOfSites ({ commit }) {
      await axios.get(this.state.dataUrl)
        .then(function (response) {
          commit('SET_SITES', response.data)
          commit('SET_MESSAGE_HANDLER', response.data)
        })
    },
    blockMessage ({ commit }, site) {
      commit('SET_BLOCK_MESSAGE_STATUS', site)
    },
    updateMessageStatus ({ commit }, site) {
      if (this.state.messageHandler[site.domain].status !== 'blocked') {
        if (this.state.messageHandler[site.domain].count + 1 > 3) {
          commit('SET_BLOCK_MESSAGE_STATUS', site)
        } else {
          commit('INCREASE_MESSAGE_COUNT', site)
        }
      }
    },
    clear ({ commit }) {
      commit('CLEAR_MESSAGE_HANDLER')
    }
  }
})
