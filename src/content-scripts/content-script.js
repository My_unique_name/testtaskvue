import Vue from 'vue'
import Message from '../components/Message.vue'

chrome.runtime.onMessage.addListener(function (message) {
  if (message.action === 'show_message') {
    const el = document.createElement('div')
    el.setAttribute('id', 'el')
    document.body.append(el)
    new Vue({
      render: h => h(Message, { props: { siteInfo: message.data } })
    }).$mount('#el')
  }
})
