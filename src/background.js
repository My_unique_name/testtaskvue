import { store } from './store'

async function initialize () {
  await store.dispatch('loadListOfSites')
  setTimeout(async function load () {
    await store.dispatch('loadListOfSites')
    setTimeout(load, 3600000)
  }, 3600000)
}

initialize()

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
  if ('status' in changeInfo && changeInfo.status === 'complete' && tab.url) {
    const [currentSite] = store.state.sites.filter(site => tab.url.indexOf(site.domain) !== -1)
    if (currentSite) {
      store.dispatch('updateMessageStatus', currentSite)
      if (store.state.messageHandler[currentSite.domain].status !== 'blocked') {
        chrome.tabs.sendMessage(tabId, { action: 'show_message', data: currentSite })
      }
    }
  }
})

chrome.runtime.onMessage.addListener(function (request) {
  if (request.action === 'message_closed') {
    store.dispatch('blockMessage', request.data)
  }
})

chrome.windows.onRemoved.addListener(function () {
  store.dispatch('clear')
})
